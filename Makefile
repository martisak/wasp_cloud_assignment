
all: instances.png

sparklog.log: commands.sh linalg-measure.py
	./commands.sh

data: crystm03 adj1138

crystm03:
	wget https://sparse.tamu.edu/MM/Boeing/crystm03.tar.gz
	tar xvzf crystm03.tar.gz

adj1138:
	wget https://sparse.tamu.edu/MM/HB/1138_bus.tar.gz
	tar xvzf 1138_bus.tar.gz

cleanlog.csv: sparklog.log
	cat sparklog-*.log | grep helloworld | grep INFO | perl -pe 's/\x1b\[[0-9;]*[mG]//g' | cut -d'-' -f2 | grep -v tmp > cleanlog.csv
	cat sparklog-larger-*.log | grep helloworld | grep INFO | perl -pe 's/\x1b\[[0-9;]*[mG]//g' | cut -d'-' -f2 | grep -v tmp > cleanlog-larger.csv

instances.png: cleanlog.csv plot.R data
	Rscript plot.R
	Rscript plot-larger.R

report:
	make -C report