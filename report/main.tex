\documentclass[a4paper]{scrartcl}
\usepackage[utf8]{inputenc}

\usepackage{todonotes}
\usepackage{color}

\usepackage[font={small}, labelfont=bf]{caption}
\usepackage{subcaption}

\usepackage{siunitx}

\usepackage{listings}
\usepackage{url}
\usepackage{hyperref}

\hypersetup{
    %draft, % Uncomment to remove all links (useful for printing in black and white)
    colorlinks=true, breaklinks=true, bookmarks=true,bookmarksnumbered,
    urlcolor=EricssonBlue, linkcolor=EricssonBlue, citecolor=EricssonGreen, % Link colors
    pdftitle=WASP Cloud Assignment Report % PDF title
}

\usepackage[acronym,toc,shortcuts]{glossaries}
\usepackage{glossary-longbooktabs}
\setglossarystyle{long-booktabs}
% \glsdisablehyper % Removes hyperlinks on defined terms and symbols
\makeglossaries
\renewcommand*\glspostdescription{\dotfill}
\renewcommand{\glsnamefont}[1]{\textbf{#1}}
\renewcommand*{\glsgroupskip}{}
\usepackage{cleveref}
\usepackage[affil-it]{authblk}

\usepackage{graphicx}

\usepackage{tikz, adjustbox}
\usetikzlibrary{shapes.geometric, arrows, decorations.pathreplacing, calc}

\tikzstyle{startstop} = [circle, rounded corners, minimum width=1cm, minimum height=1cm,text centered, draw=none, fill=EricssonOrange]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=none, fill=EricssonOrange2, text width=3cm]
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, text width=3cm, draw=none, fill=EricssonOrange1]
\tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=1cm, text centered, draw=none, fill=EricssonOrange4, text width=2cm]
\tikzstyle{arrow} = [ultra thick,->,>=stealth, shorten <=.15cm,shorten >=.15cm, rounded corners=.15cm]
\tikzstyle{brace} = [decorate, decoration={brace,amplitude=10pt, mirror}, xshift=-3cm,yshift=0pt, text width=3cm, align=right , ultra thick]

\tikzstyle{measure} = [fill=EricssonBlue1]

\title{WASP Cloud Assignment Report}

\author[1,3]{ Karl Norrman\thanks{knorrman@kth.se}}
\author[1]{Md Sakib Nizam Khan\thanks{msnkhan@kth.se}}
\author[1,2,3]{Martin~Isaksson\thanks{martisak@kth.se}}

\affil[1]{KTH Royal Institute of Technology, School of Electrical Engineering and Computer Science}
\affil[2]{RISE AI}
\affil[3]{Ericsson Research}

\usepackage[graph, grays]{ericolors}

% Primary colors
\definecolor{kth-blue}{RGB/cmyk}{25,84,166/0.849,0.494,0,0.349}
\definecolor{kth-red}{RGB/cmyk}{157,16,45/0,0.898,0.713,0.384}
\definecolor{kth-green}{RGB/cmyk}{98,146,46/0.329,0,0.685,0.427}
% Secondary colors
\definecolor{kth-lightblue}{RGB/cmyk}{36,160,216/0.833,0.259,0,0.153}
\definecolor{kth-lightred}{RGB/cmyk}{228,54,62/0,0.763,0.728,0.106}
\definecolor{kth-lightgreen}{RGB/cmyk}{176,201,43/0.124,0,0.786,0.212}
% Tertiary colors (yet more colors)
\definecolor{kth-pink}{RGB/cmyk}{216,84,151/10,0.611,0.301,0.153}
\definecolor{kth-yellow}{RGB/cmyk}{250,185,25/0,0.26,0.9,0.0196}
\definecolor{kth-darkgray}{RGB/cmyk}{101,101,108/0.0648,0.0648,0,0.576}
\definecolor{kth-middlegray}{RGB/cmyk}{189,188,188/0,0.00529,0.00529,0.259}
\definecolor{kth-lightgray}{RGB/cmyk}{227,229,227/0.00873,0,0.00873,0.102}

\lstset{ %
  float,
  basicstyle=\footnotesize\ttfamily,
  belowcaptionskip = \baselineskip,
  breakatwhitespace=true,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  % prebreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\rhookswarrow}},
  % postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\rcurvearrowse\space}},
  frame=tb,                    % adds a frame around the code
  framesep=5pt,
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{EricssonBlack}, % the style that is used for the line-numbers
  rulecolor=\color{EricssonGray4},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=true,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  tabsize=2,                       % sets default tabsize to 2 spaces
  numberstyle=\tiny\color{EricssonGray4},
    keywordstyle=\bfseries\color{EricssonBlue},
    keywordstyle={[2]\color{EricssonGreen}},
    keywordstyle={[3]\color{EricssonRed}},
keywords=[3]{@},
commentstyle=\color{EricssonGray3},
stringstyle=\color{EricssonGray2},
  %xrightmargin=-2cm,
  %xleftmargin=-1cm,
  % backgroundcolor=\color{mycolor2}   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
}

\usepackage{etoolbox}
\BeforeBeginEnvironment{lstlisting}{\par\noindent\begin{minipage}{\linewidth}}
\AfterEndEnvironment{lstlisting}{\end{minipage}\par\addvspace{\topskip}}

\date{May 2019}

\usepackage[numbers]{natbib}
\usepackage{graphicx}

\usepackage{todonotes}
\newcommand{\knote}[1]{\todo[inline,
                             bordercolor=white,
                             color=EricssonOrange,
                             size=\footnotesize
                            ]{\textsf{\textbf{Karl:} #1}}}
\newcommand{\mnote}[1]{\todo[inline,
                             color=EricssonGreen,
                             bordercolor=white,
                             size=\footnotesize
                            ]{\textsf{\textbf{Martin:} #1}}}
\newcommand{\snote}[1]{\todo[inline,
                             color=kth-blue,
                             bordercolor=white,
                             size=\footnotesize
                            ]{\textsf{\textbf{Sakib:} #1}}}

\newcommand{\nm}[1]{\textsc{#1}}
\newcommand{\clcmd}[1]{\texttt{#1}}
\newcommand{\timing}[1]{\texttt{#1}}

\newacronym{API}{API}{Application programming interface}
\newacronym{IID}{IID}{Independent and identically distributed}
\newacronym{KTH}{KTH}{KTH Royal Institute of Technology}
\newacronym{MPC}{MPC}{Multi Party Computation}
\newacronym{PCA}{PCA}{Principal component analysis}
\newacronym{RDD}{RDD}{Resilient Distributed Dataset}
\newacronym{SVD}{SVD}{Singular Value Decomposition}
\newacronym{UAV}{UAV}{Unmanned Aerial Vehicle}
\newacronym{URI}{URI}{Uniform Resource Identifier}
\newacronym{USV}{USV}{Unmanned Surface Vehicle}
\newacronym{WARA-PS}{WARA-PS}{WARA Public Safety}
\newacronym{WARA}{WARA}{WASP Autonomous Research Arenas}
\newacronym{WASP}{WASP}{Wallenberg AI, Autonomous Systems and Software Program}

\newglossaryentry{worker} {
    first = {\emph{worker}},
    firstplural = {\emph{workers}},
    name = {worker},
    plural = {workers},
    description = {A worker is an abstraction of a computing machine in the
    cluster. It is an infrastructure concept.}
}
\newglossaryentry{job} {
    first = {\emph{job}},
    firstplural = {\emph{jobs}},
    name = {job},
    plural = {jobs},
    description = {A job is a \nm{Spark} concept representing one run of the
    experiment in the context of this report.}
}
\newglossaryentry{session} {
    first = {\emph{session}},
    firstplural = {\emph{sessions}},
    name = {session},
    plural = {sessions},
    description = {A \nm{Spark} Session~\cite{sparksession} is the new entry point
    to programming \nm{Spark} from which we can get a \nm{Spark} Context that we
    can use to access the \ac{RDD} \ac{API}.}
}
\newglossaryentry{executor} {
    first = {\emph{executor}},
    firstplural = {\emph{executors}},
    name = {executor},
    plural = {executors},
    description = {An executor is a kind of process executing a piece of work.
        It is a \nm{Spark} concept and many executors can run on each
        \gls{worker}~\cite{sparkcluster}.}
}
\newglossaryentry{data} {
    first = {\emph{data}},
    firstplural = {\emph{data}},
    name = {data},
    plural = {data},
    description = {The data is the input on which the matrix operations are
        performed.}
}
\newglossaryentry{sample} {
    first = {\emph{sample}},
    firstplural = {\emph{samples}},
    name = {sample},
    plural = {samples},
    description = {A sample is in the context of this report a measurement of
        time taken to execute one or more steps in the pipeline.}
}

\newglossaryentry{client} {
    first = {\emph{client}},
    firstplural = {\emph{clients}},
    name = {client},
    plural = {clients},
    description = {The client runs on a local machine and starts the
    \gls{driver} program by submitting a \gls{job} to the cluster.}
}

\newglossaryentry{driver} {
    first = {\emph{driver}},
    firstplural = {\emph{drivers}},
    name = {driver},
    plural = {drivers},
    description = {The driver process is running the main function and is
    creating the \nm{Spark}~\gls{session}~\cite{sparkcluster}. Runs on a
    \gls{worker}.}
}

\newglossaryentry{application} {
    first = {\emph{application}},
    firstplural = {\emph{applications}},
    name = {application},
    plural = {application},
    description = {The highest level unit of computation
    in \nm{Spark}~\cite{sparkapplication,sparkcluster}, }
}

\begin{document}

\maketitle

\begin{abstract}
    We evaluate the execution speed of two matrix operations from the
    \nm{Spark} \nm{LINALG} package when executed with varying number of
    executors on the Google Cloud Platform.
    %
    Our results indicate that, while there in some cases are an improvement,
    there are many factors that affect the computation and one cannot expect
    speed-ups for an arbitrary given problem.
\end{abstract}

\cleardoublepage
\setcounter{tocdepth}{3}   % number of levels in ToC
\pagenumbering{roman}

\setcounter{page}{1}
\tableofcontents % Always compile twice

\clearpage
\addcontentsline{toc}{section}{List of Figures}
\listoffigures

\addcontentsline{toc}{section}{List of Tables}
\listoftables

\clearpage
\pagenumbering{arabic}
\setcounter{page}{1}     % Ensure this is in page 1
\pagestyle{plain}
\section{Introduction}
This report discusses the assignment of the cloud module in the
\textit{\acs{WASP} Software Engineering and Cloud Computing} course.
%
The project aims to evaluate the use of
\nm{Spark}~\cite{DBLP:journals/cacm/ZahariaXWDADMRV16} on a cloud platform for
improving the performance of matrix operations when adding more
\glspl{executor}.
%
Such an evaluation gives a sense of how well the library scales when
parallelizing the operations, and indirectly a rough sense of improvements one
can expect in general when moving computation from a single local machine to
the cloud.
%
The specific \nm{Spark} library evaluated is
\nm{Linalg}~\cite{orgapach47:online,DBLP:conf/kdd/ZadehMUYPVSSZ16}.
%

To execute the evaluation we first set up and configured a cluster at Google
Cloud Platform (see Section~\ref{sec:platformSetup}).
%
Next, we selected two matrix operations to evaluate, \ac{SVD} and \ac{PCA} (see
Section~\ref{sec:methChoice}), and implemented these in \nm{Spark} (see
Section~\ref{sec:spark}).
%
Since distributed computation induces an overhead in terms of start-up and
allocation costs for resources, as well as in communication to synchronize
\gls{data} across the computing \glspl{executor}, we had to experiment with
various sizes of input \gls{data}.
%
This is elaborated on in Section~\ref{sec:dataSelect}.
%
The report concludes with reflections and learnings from the project in
Section~\ref{sec:conc}.
%

\section{Experiment}
\subsection{Cloud Platform Setup}
\label{sec:platformSetup}
For the project, Google Cloud Platform was recommended.
%
To configure the cluster we used the \clcmd{gcloud}~\cite{dataproc} console
application with Google \nm{Dataproc} beta~\cite{dataprocbeta}~\ac{API}. The beta
\ac{API} allows us to set a maximum idle-time on our cluster.
%
We wrote a shell script to automate the platform setup, cluster creation and
configuration, as well as executing the \nm{Spark} \gls{job}.
%

A portion of the script is shown in \Cref{commandsnippet}. This sets up the
Google \nm{Dataproc} cluster with the maximum permitted number of \glspl{worker}
(for a free account).
%
We keep this fixed, and vary the number of \nm{Spark} \glspl{executor}.
%
The cluster is started once, and kept running for
the duration of the experiment. We have seen that restarting the cluster
affects the measurements --- the reported numbers are for one instance of
a cluster.
%
\lstinputlisting[language=Bash, firstline=20, lastline=34,
                 caption={A script file to setup a Google
                     \nm{Dataproc} cluster~\cite{dataproc}
                 and submit the \nm{Spark} job. See \ref{commands} for the full
                 listing. This section sets up a cluster of
                 5~\texttt{n1-standard-1} machines with additional Python
                 packages.}, label=commandsnippet]{../commands.sh}

The code can be found in its entirety
on \url{https://gitlab.com/martisak/wasp_cloud_assignment} and in the appendix.
%

\subsection{Choice of Matrix Operations}
\label{sec:methChoice}
The \nm{Linalg} library provides multiple matrix operations.
%
We chose to evaluate \ac{SVD}~\cite{strang1993introduction} and
\ac{PCA}~\cite{Hastie2017}, which both are related to
reduction of the number of dimensions.
%
Since we were mainly interested in how the computation time scales with
increasing number of \glspl{executor}, the actual results of
these operations were not considered important.
%

\subsection{Matrix Selection}
\label{sec:dataSelect}
To find an appropriately sized matrix to work with, we started by running
\ac{SVD} and \ac{PCA} on a very small matrix, and increased the size until
we started to see differences in time measured for various numbers of
\glspl{executor}.
%
Following this process, we ended up with a matrix from the Power systems
admittance matrices set~\cite{1138_bus} and~\Cref{fig:1138}.
%
We will refer to this as the \emph{Power Network Problem}.
%
This matrix is very sparse, containing only \num{2596}~values (before making
it symmetric).
%
Thus, only about \SI{0.37}{\percent} of the elements are non-zero.
%
It is also ill-conditioned, see~\Cref{tbl:datasets}.
%
% \begin{table}
% \centering
% \caption[Matrix comparison]{Comparison between the two matrices used in this report.}
% \label{tbl:datasets}
% \begin{tabular}{lll}
% \toprule
% \textbf{Parameter} & \textbf{1138\_bus~\cite{1138_bus}} & \textbf{crystm03~\cite{boeing}} \\
% \midrule
% \textbf{Shape} & \num{1138x1138} & \num{24696x24696} \\
% \textbf{Kind} & Power Network Problem & Materials Problem\\
% \textbf{Non-zeros} &  \num{4054}  & \num{583770}\\
% \textbf{Condition Number} & \num{8.572646e6} & \num{2.640325e2}\\
% \bottomrule
% \end{tabular}
% \end{table}

\begin{table}
\centering
\caption[Matrix comparison]{Comparison between the two matrices used in this report.}
\label{tbl:datasets}
\begin{tabular}{lSSS}
\toprule
\textbf{Matrix} & \textbf{Shape} & \textbf{Non-zeros} & \textbf{Condition Number}\\
\midrule
Power Network Problem~\cite{1138_bus} & \num{1138x1138} & \num{4054} &  \num{8.572646e6} \\
Materials Problem matrix~\cite{boeing} & \num{24696x24696} & \num{583770} & \num{2.640325e2} \\
\bottomrule
\end{tabular}
\end{table}

\begin{figure}[htbp]
    \centering
    \begin{subfigure}[t]{0.49\textwidth}
    \includegraphics[width=\textwidth]{images/1138adj}
    \caption[Power Network Problem Adjacency Matrix]{Adjacency matrix from the Power systems admittance matrices
        set~\cite{1138_bus}. Since the matrix is symmetrical, the \gls{data}
        only contains half the matrix, i.e.\, it is upper triangular.}
    \label{fig:1138}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.49\textwidth}
    \includegraphics[width=\textwidth]{images/crystm03.pdf}
    \caption[Fem Crystal Free Vibration Mass Matrix]{Fem Crystal Free Vibration Mass Matrix~\cite{boeing}. Since this matrix is also symmetrical, the \gls{data}
        only contains half the matrix, i.e.\, it is upper triangular.}
    \label{fig:crystm03}
    \end{subfigure}
    \caption[Visualization of the sparse matrices]{The two matrices we are performing computations on are presented
        here. The plots illustrate the sparsity of these matrices, and as
        expected the diagonal elements, and the elements close to the diagonal
        are present in the \gls{data}.}
\end{figure}

We have also used another matrix, the Fem Crystal Free Vibration
Mass Matrix~\cite{boeing}.
%
This matrix is larger, but the structure is very
different from the first matrix.
%
We will refer to this as the \emph{Materials Problem matrix}.
%
It is visualized in~\Cref{fig:crystm03} and some
interesting properties such as the condition number can be
seen in~\Cref{tbl:datasets}.
%

\section{Implementation}
\label{sec:spark}
%
We now describe how our pipeline, implemented using \nm{Spark}'s Python
bindings, is constructed.
%
The pipeline is depicted in~\Cref{fig:pipeline}.
%

\begin{figure}
\centering
\begin{adjustbox}{width=1\linewidth}
\begin{tikzpicture}[node distance=2cm]

\node (start) [startstop] {Start};
\node (cluster) [io, below of=start] {Instantiate Google \nm{Dataproc} cluster};
\node (submit) [process, below of=cluster] {Submit \nm{Spark} \gls{job}};
\node (in1) [io, measure, below of=submit] {Download and read in the data};
\node (pro1) [process, measure, below of=in1] {Create the \nm{Spark} session};
\node (pro2) [process, measure, below of=pro1] {Parallelize data in the cluster};
\node (pro3) [process, measure, below of=pro2] {Perform matrix computations};
\node (pro4) [process, measure, below of=pro3] {Stop \nm{Spark} session};

\node (dec1) [decision, measure, below of=pro4, yshift=-1cm] {Generate more data?};

\node (pro2a) [process, below of=dec1, yshift=-1cm] {Generate figures};
\node (pro2b) [decision, measure, right of=dec1, xshift=2cm] {Increase executors?};
\node (pro2c) [process, right of=pro2b, xshift=2cm] {Stop \nm{Spark} job};

\node (out1) [io, below of=pro2a] {Generate report};
\node (stop) [startstop, below of=out1] {Stop};

\draw [arrow] (start) -- (cluster);
\draw [arrow] (cluster) -- (submit);
\draw [arrow] (submit) -- (in1);
\draw [arrow] (in1) -- (pro1);
\draw [arrow] (pro1) -- (pro2);
\draw [arrow] (pro2) -- (pro3);
\draw [arrow] (pro3) -- (pro4);
\draw [arrow] (pro4) -- (dec1);
\draw [arrow] (dec1) -- node[anchor=east] {no} (pro2a);
\draw [arrow] (dec1) -- node[anchor=south] {yes} (pro2b);
\draw [arrow] (pro2b) -- node[anchor=south] {yes} (pro2c);
\draw [arrow] (pro2b) |- node[anchor=south] {no} (pro1);
\draw [arrow] (pro2c) |- (submit);
\draw [arrow] (pro2a) -- (out1);
\draw [arrow] (out1) -- (stop);

\draw [brace] ($ (pro1.north west) + (-.2cm,0)$) -- ($ (pro1.south west) + (-.2cm,0)$) node [black,midway, xshift=-3cm, align=right]
{\timing{spark\_setup}};

\draw [brace] ($ (in1.north) + (-2.2cm,0)$)-- ($ (in1.south) + (-2.2cm,0)$) node [black,midway,xshift=-3cm, align=right]
{\timing{matrix\_read}};

\draw [brace] ($ (pro2.north west) + (-.2cm,0)$) -- ($ (pro2.south west) + (-.2cm,0)$)node [black,midway,xshift=-3cm, align=right]
{\timing{spark\_rdd} + \timing{coordinate\_matrix} + \timing{row\_matrix}};

\draw [brace] ($ (pro3.north west) + (-.2cm,0)$)-- ($ (pro3.south west) + (-.2cm,0)$) node [black,midway,xshift=-3cm, align=right]
{\timing{svd} + \timing{pca}};

\draw [brace] ($ (in1.north) + (-7.2cm,0)$) -- ($ (dec1.south) + (-7.2cm,0)$)node [black,midway,xshift=-3cm, align=right]
{\texttt{linalg-measure.py}};

\draw [brace] ($ (start.north) + (-12.2cm,0)$) -- ($ (dec1.south) + (-12.2cm,0)$)node [black,midway,xshift=-3cm, align=right]
{\texttt{commands.sh}};

\draw [brace] ($ (pro2a.north) + (-7.2cm,0)$) -- ($ (pro2a.south) + (-7.2cm,0)$)node [black,midway,xshift=-3cm, align=right]
{\texttt{plot.R}};

\end{tikzpicture}
\end{adjustbox}

\caption{Our \nm{Spark} pipeline.}
\label{fig:pipeline}
\end{figure}

The pipeline represent an execution of the experiment, which consists of a
number of steps.
%

\subsection{Cluster Setup and Launching the Job}
%
The first step is to create a cluster of \glspl{worker} on the cloud platform,
where a \gls{worker} is and abstraction of a computing machine.
%
Exactly how a \gls{worker} is realized in actual physical resources in the
cloud is not entirely clear from the information we have.
%
When the cluster is set up, we launch a \nm{Spark} \gls{job} that measures the
running time of matrix operations.
%
The \gls{job} is written as a Python script that spawns a number of \nm{Spark}
\glspl{executor}, which can be seen as processes performing a part of the work.
%
While not clear to us from the documentation, we assume that \glspl{executor}
are spread efficiently across \glspl{worker} by the cloud platform.
%

\subsection{Data Retrieval}
%
The \nm{Spark} \gls{driver} downloads the \gls{data} from the given \ac{URI},
extracts it and loads it into memory.
%
The data is stored on the server in Matrix Market
format~\cite{matrixmarketformat}.
%
%The matrix we chose is an adjacency matrix from a Power Network
%Problem~\cite{1138_bus}.
%
Our script reads it in textual form and the time it took to read it is shown as
\timing{matrix\_read} in~\Cref{fig:instances}.
%

\subsection{\nm{Spark} Session Management}
%
Each iteration of the loop creates a new \nm{Spark} \gls{session} and uses a
different number of \glspl{executor}.
%
At the outset of an iteration, a fresh \gls{session} \texttt{SparkSession} and
\texttt{SparkContext} are created.
%
We set \texttt{spark.executor.instances} equal to the number of
\glspl{executor} to be used.
%
This prevents \nm{Spark} from dynamically allocating \glspl{executor},
which enables us to precisely control the number of \glspl{executor} for the
\gls{session}.
%
The setup time is recorded and the measurements from the experiment we ran are
denoted \timing{spark\_setup} in~\Cref{fig:instances}.
%

\subsection{Data Conversion}
%
We read the downloaded \gls{data} into an \ac{RDD} in the format
\texttt{(row, column, \gls{data})}, where entries are in in text representation
(see~\Cref{rddmanip}).
%

Because we use \nm{Spark} to convert the data, we partition it to take advantage
of \nm{Spark}'s parallelization capabilities before feeding it to the
constructors of the distributed matrix classes.
%
We set the number of partitions to be twice the number of
\glspl{executor}~\cite{sparkprogguide}. The partitioning is however only
applies to the input data in the sparse \ac{RDD} representation.
The distributed matrices (\texttt{CoordinateMatrix} and
\texttt{RowMatrix}) will probably be partitioned differently.

The number of partitions used during the construction of the matrices is a
hyper-parameter that should be tuned for best performance.

Then we convert this into a
\texttt{CoordinateMatrix}, which is used to store distributed sparse matrices,
see~\Cref{coordmatrix}.
%
After that we convert the \texttt{CoordinateMatrix} into a
\texttt{RowMatrix}, a row-oriented distributed matrix.
%

\lstinputlisting[language=Python, firstline=132, lastline=140,
                 caption={\ac{RDD} manipulation. Since the \gls{data} is only for
                 half the matrix, we also make it copy one half to the other,
                 minus the diagonal which otherwise would be duplicated.
                 We need an a \texttt{reduceByKey} step to deal with duplicate
                 entries in this matrix. A sorted \ac{RDD} makes
                 conversion to \texttt{CoordinateMatrix} faster.},
            label=rddmanip]{../linalg-measure.py}

\lstinputlisting[language=Python, firstline=148, lastline=158,
                 caption={\ac{RDD} to \texttt{CoordinateMatrix} and to
                 \texttt{RowMatrix} conversion},
            label=coordmatrix]{../linalg-measure.py}

These preprocessing steps are timed and shown as \timing{spark\_rdd},
\timing{coordinate\_matrix} and \timing{row\_matrix} in~\Cref{fig:instances}.
%

\subsection{Matrix Operations}
%
Each iteration computes both \ac{SVD} and \ac{PCA} on the \texttt{RowMatrix},
and measures the execution time of each operation.
%
The measurements are shown as \timing{svd} and \timing{pca} respectively
in~\Cref{fig:instances}.
%

We measure the time for each iteration to see the difference adding another
\gls{executor} makes.
%
% Karl: this is not correct according to the python code:
%For each number of \glspl{executor}, we a new \nm{Spark} job
%and run only a few iterations to avoid memory overflow issues.
%
We run only once for each number of \glspl{executor} to avoid memory overflow
issues, and measure the time as a \gls{sample}.
%
To accumulate enough \glspl{sample}, we run the pipeline many times.
%
If the cluster caches \gls{data} between iterations, time measurements may not
be accurate.
%
Clearly, we cannot ensure that all caches are flushed and all of the
allocated memory is freed between iterations.
%
Creating a new \nm{Spark} \gls{session} for each iteration is the best way we
could find to avoid caching-effects; it also mitigates the memory allocation
problems to some degree, see~\Cref{sec:results}. This also had the positive
effect of interleaving the \gls{data} generation which potentially reduced the
impact of short-lived interference in our measurements due to other
processes running in the system that we do not have control over.
%

\section{Results and Analysis}
\label{sec:results}
%
Using a large matrix resulted in longer running times as expected,
but also in memory overflow failures.
%
We found that, in addition to creating a fresh \nm{Spark}~\gls{session} for each
iteration, creating and destroying fewer \nm{Spark}
\glspl{session} per submitted \nm{Spark} \gls{job} further alleviates the
problem.
%
Therefore, for each fixed-size set of \glspl{executor}, we ran
five~\glspl{session}, obtaining five~\glspl{sample}.
%
We repeated this whole process \num{10}~times to reach \num{50}~\glspl{sample} per
set of \glspl{executor} in total.

%
During the experiment we had two~failures in the \timing{pca} step related
to failure to allocate memory.
%
We re-executed the failed \glspl{job}, ending up with
\num{51}~\glspl{sample} for \num{8}~and \num{9}~\glspl{executor} respectively for the
initial steps, and 50~\glspl{sample} for the \timing{pca} step.
%
\Cref{fig:instances} depicts the elapsed time for each of the
individual steps, and \Cref{fig:sum} depicts the total elapsed time
for the complete \gls{job} of running both \ac{SVD} and \ac{PCA}.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=1\textwidth]{images/instances}
    \caption[Power Network Problem --- Elapsed time versus number of%
    \glspl{executor}]%
        {Elapsed time for the S Admittance Matrix 1138 Bus Power
        System matrix~\cite{1138_bus} versus number of \glspl{executor} for
        each timed step.
        The \texttt{read\_matrix} step is run on the \gls{driver} regardless
        of the number of \glspl{executor}.}
    \label{fig:instances}
\end{figure}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=1\textwidth]{images/instances-larger}
    \caption[Materials Problem --- Elapsed time versus number of \glspl{executor}]%
        {Elapsed time for the Fem Crystal Free Vibration Mass
        Matrix~\cite{boeing} versus number of \glspl{executor} for each timed
        step.
        The \texttt{read\_matrix} step is run on the \gls{driver} regardless of the
    number of \glspl{executor}.
    For this problem we were unable to run \ac{PCA}, and therefore the
    \timing{pca} step is missing in this plot.}
    \label{fig:instances2}
\end{figure}

\begin{figure}[htbp]
 \centering
 \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/sum}
    \caption[Power Network Problem --- total elapsed time]{Total elapsed time for the S Admittance Matrix 1138 Bus Power
    System matrix~\cite{1138_bus}.}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.49\textwidth}
     \centering
     \includegraphics[width=\textwidth]{images/sum-larger}
     \caption[Materials Problem ---  total elapsed time]{Total elapsed time for the Fem Crystal Free Vibration
     Mass Matrix~\cite{boeing}. Note that this does not
     include the \timing{pca} step.}
     \label{fig:boeingsum}
\end{subfigure}
\caption[Total elapsed time for the Power Network Problem and the Materials Problem]{Total elapsed time versus number of \glspl{executor}. The grey area shows
the region where we have more than one \nm{Spark} \gls{executor} per
\gls{worker}.}
\label{fig:sum}

\end{figure}

The \texttt{read\_matrix} step is run on the \gls{driver} regardless of the
number of \glspl{executor} and hence it is plotted separately.
%

\texttt{spark\_setup} takes about the same time regardless of the number of
\glspl{executor}\footnote{On other systems with \nm{Spark} in \nm{Kubernetes}
and heterogeneous deployments we saw an increase in the setup time with the
number of \glspl{executor}.
%
This was because, in those systems, a new pod is spawned for each new
\gls{executor} and the slowest \gls{worker} affects the time the most.}.
%
Here we have already setup the \glspl{worker} beforehand and
only select how many \glspl{executor} to use in this iteration.
%
In \texttt{spark\_rdd} step we read in the \gls{data}, made the matrix symmetric
and removed duplicates.
%
There were duplicates in this \gls{data}, that had to be removed.
%
Here we also sorted the \gls{data} to reduce the total time for the later
\ac{SVD} and \ac{PCA} steps.
%
Interestingly, the time increases with the number of added \glspl{executor},
except for a small dip with \num{4}~\glspl{executor}.
%
In the \timing{coordinate\_matrix}, \timing{svd}, and \timing{pca} steps
in~\Cref{fig:sum}, we see an increase in processing time with more
\glspl{executor}.
%
Also, as evident from~\Cref{fig:sum}, increasing the number of
\glspl{executor} beyond the number of \glspl{worker} in the cluster increase
execution time.
%

For the case of the larger Materials Problem matrix, the elapsed time per step
can be seen in~\Cref{fig:instances2}.
%
The total elapsed time seen
in~\Cref{fig:boeingsum} decreased initially but
then increased.
%
The biggest contributor is the \timing{svd} step.
%
For this matrix we were unable to run \timing{pca} due to memory issues.
%

\section{Conclusions, Reflections and Learnings}
\label{sec:conc}
We draw two main conclusions from this study.
%
First, there is a relatively large setup cost for a computation in the cloud,
at least for the platform and libraries used.
%
This means that the benefits of using the cloud for computation only appear for
large problem instances, large sets of smaller problem
instances that can be handled in batches, or algorithms with high computational
complexity.
%

Second, while hiding the underlying infrastructure from the programmer via
abstraction is nice from a program construction perspective, it turned out that
in practice it became very troublesome to debug.
%
These problems materialized in the form of spurious out of memory errors,
computations being preempted etc.
%

Abstracting away the underlying hardware also makes measurements difficult.
%
While we in one case can see positive effects on the execution time when
increasing the number of \glspl{worker}, it is difficult to tell whether
certain \glspl{executor} ran on the same core, whether the core has been
artificially capped etc.
%
On the other hand, given the purpose of the abstraction it is even questionable
whether such information \emph{should} leak to the programmer at the \nm{Spark}
layer.
%
It would be beneficial with an \ac{API} in \nm{Spark} obtain execution speed
in a meaningful measure independent of the underlying infrastructure.
%

To get good performance in a \nm{Spark} pipeline it is important to
understand not only the framework, but also how the \gls{data} affects the
computation.
%
This is evident from our results in this study, where we
compared the processing time in a pipeline for two matrices.
%
For example, it is important to design the pipeline such that have as few
shuffle operations as possible.
%

While the project did not reveal any unexpected results, it was useful to get
a little hands-on experience with cloud computing and the difficulties involved.
%

\section{Contributions}
%
All three group members contributed equally in all parts of the assignment.
%
In the implementation phase, we sat together and implemented the project.
%
For the testing phase, we divided our work based on different cluster
configuration, functions and also matrix sizes and ran tests individually.
%

The report was written iteratively and in parallel, with equal contributions
from all members.
%

\section*{Acknowledgment}
This work was partially supported by the \acf{WASP} funded by the Knut and
Alice Wallenberg Foundation.
%

\clearpage
\printglossaries

\clearpage
\bibliographystyle{IEEEtranSN}
\bibliography{references}

\cleardoublepage{}
\appendix
\section{Appendix --- Source code}
The code can be found in its entirety on
\url{https://gitlab.com/martisak/wasp_cloud_assignment}.

\subsection{Makefile}
\lstinputlisting[language=make]{../Makefile}

\subsection{Shell script}
\label{commands}
\lstinputlisting[language=Bash]{../commands.sh}

\subsection{LINALG time measurements}
\lstinputlisting[language=Python]{../linalg-measure.py}

\subsection{Plotting script}
\lstinputlisting[language=R]{../plot.R}

\end{document}
