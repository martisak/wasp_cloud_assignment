#!/bin/sh

# Parameters
PROJECT=`gcloud config list --format 'value(core.project)'`
CLUSTER=basil
REGION=europe-west3
ZONE=${REGION}-a
NUMWORKERS=5
# SOURCE=linalg-measure.py
SOURCE=linalg-measure-larger-matrix.py
WORKER_MACHINE_TYPE=n1-standard-1
DRIVER_MACHINE_TYPE=$WORKER_MACHINE_TYPE
LOG_PREFIX=sparklog-larger

# Create a cluster with NUMWORKERS non-preemptible workers,
# that live 30 minutes at most from the last command.
# Preemptible workers can't have GPUs.

if [[ ! $(gcloud dataproc clusters list | grep $CLUSTER) ]]; then
    gcloud beta dataproc clusters create $CLUSTER \
        --max-idle=30m \
        --zone $ZONE \
        --master-machine-type $DRIVER_MACHINE_TYPE \
        --master-boot-disk-size 20 \
        --num-workers $NUMWORKERS \
        --worker-machine-type $WORKER_MACHINE_TYPE \
        --metadata 'PIP_PACKAGES=scipy==1.2.1 colorlog==3.1.0' \
        --worker-boot-disk-size 20 \
        --project $PROJECT \
        --image-version 1.4-ubuntu18 \
        --initialization-actions \
         gs://dataproc-initialization-actions/python/pip-install.sh
fi

# List the clusters to see if it's alive.
gcloud dataproc clusters list

for n in {1..10}
do
    for n in {1..10}
    do
        # Run the Spark application
        gcloud dataproc jobs submit pyspark --cluster $CLUSTER $SOURCE -- $n 2>&1 | tee -a $LOG_PREFIX-${n}.log
    done
done

# Delete the cluster
# gcloud dataproc clusters delete --quiet $CLUSTER

