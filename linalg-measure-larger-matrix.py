#!/usr/bin/python

import colorlog
import gc
import logging
import os
import pyspark
import sys
import tarfile
import timeit
import urllib.request
import uuid
from pyspark.mllib.linalg.distributed import CoordinateMatrix, MatrixEntry
from pyspark.sql import SparkSession

# Handle loggers

def get_logger(name, level=logging.DEBUG):
    """
    This function returns a logger object. It removes existing handles,
    and sets the propagation flag to False to avoid double outputs.
    """

    logger = colorlog.getLogger(name)
    while logger.handlers:
        logger.handlers.pop()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.propagate = False

    return logger

format = '%(log_color)s%(asctime)15s %(name)15s %(levelname)10s - %(message)s'
handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter(
    format, datefmt="%y%m%d %H:%M:%S"))

# Set log level for Spark logs to WARN
for logname in ["py4j", "pyspark"]:

    logger = logging.getLogger(logname)

    while logger.handlers:
        logger.handlers.pop()

    logger.addHandler(handler)
    logger.setLevel(logging.WARN)
    logger.propagate = False

root = logging.getLogger()
root.setLevel(logging.WARNING)

mylogger = get_logger("helloworld")

# Do this a number of times
instances = int(sys.argv[1])
run_id = str(uuid.uuid4())[:8]

# Start timing
start_time = timeit.default_timer()

dirpath = os.getcwd()

tar_path = os.path.join(dirpath, "crystm03.tar.gz")
url = "https://sparse.tamu.edu/MM/Boeing/crystm03.tar.gz"

mylogger.info("Download {} from {}".format(tar_path, url))

urllib.request.urlretrieve(url, tar_path)

mylogger.info("Extract {}".format(tar_path))

tar = tarfile.open(tar_path, "r:gz")
tar.extractall()
tar.close()

mtx_file = os.path.join(dirpath, "crystm03/crystm03.mtx")

mylogger.info("Read {} in Matrix Market format.".format(mtx_file))

with open(mtx_file) as f:
    data = [line.rstrip('\n') for line in f]

matrix_read_time = timeit.default_timer() - start_time

#mylogger.info("thing; iteration; instances; elapsed")
mylogger.info("{}; {}; {}; {}; {}".format(
    "matrix_read", 0, instances, matrix_read_time, run_id))

for iteration in range(5):

    # Start measuring time
    start_time = timeit.default_timer()

    # Set Spark executors. Perhaps setting instances override
    # dynamicAllocation? Check the number of cores per workers,
    # in respect to number of cores per Google Cloud worker
    # n1-standard-1.

    spark = (SparkSession
             .builder
             .master("yarn")
             .appName('dataproc-python-demo')
             .config('spark.executor.instances', str(instances))
             .config('spark.dynamicAllocation.initialExecutors', str(instances))
             .config('spark.dynamicAllocation.minExecutors', str(instances))
             .config("spark.driver.memory", "2600M")
             .config("spark.executor.memory", "2600M")
             .getOrCreate())

    sc = spark.sparkContext

    mylogger.debug(sc.getConf().getAll())

    # Write spark_setup time to log and restart timer
    spark_setup_time = timeit.default_timer() - start_time
    mylogger.info("{}; {}; {}; {}; {}".format(
        "spark_setup", iteration, instances, spark_setup_time, run_id))
    start_time = timeit.default_timer()

    # Remove comments (lines starting with %), split line and
    # mutate into (row, column, value) format.
    # Remove duplicates (take the smallest of two duplicates).
    #
    # org.apache.spark.mllib.linalg.distributed.RowMatrix:
    #    The input data is not directly cached, which may hurt performance
    #    if its parent RDDs are also uncached.

    entries = (sc.parallelize(data, 2 * instances)
                   .filter(lambda x: not x.startswith("%"))
                   .map(lambda x: x.split(" "))
                   .map(lambda x: [((int(x[0]), int(x[1])), float(x[2])), ((int(x[1]), int(x[0])), float(x[2]))])
                   .flatMap(lambda x: [((w[0][0], w[0][1]), w[1]) for w in x])
                   .reduceByKey(lambda a, b: min(a, b))
                   .map(lambda x: (x[0][0], x[0][1], x[1]))
                   .sortBy(lambda x: (x[0], x[1]))
                   ).cache()

    # Write spark_rdd time to log and restart timer
    spark_rdd_time = timeit.default_timer() - start_time
    mylogger.info("{}; {}; {}; {}; {}".format(
        "spark_rdd", iteration, instances, spark_rdd_time, run_id))
    start_time = timeit.default_timer()

    # Convert to CoordinateMatrix
    coord_matrix = CoordinateMatrix(entries)

    # Write coordinate_matrix time to log and restart timer
    coordinate_matrix_time = timeit.default_timer() - start_time
    mylogger.info("{}; {}; {}; {}; {}".format(
        "coordinate_matrix", iteration,
        instances, coordinate_matrix_time, run_id))
    start_time = timeit.default_timer()

    mat = coord_matrix.toRowMatrix()

    # Write coordinate_matrix time to log and restart timer
    row_matrix_time = timeit.default_timer() - start_time
    mylogger.info("{}; {}; {}; {}; {}".format(
        "row_matrix", iteration, instances, row_matrix_time, run_id))
    start_time = timeit.default_timer()

    #---------------------------SVD-----------------------------------
    # "At most k largest non-zero singular values and associated
    # vectors are returned". k = 5
    # https://spark.apache.org/docs/2.1.0/api/java/org/apache/spark/mllib/linalg/distributed/RowMatrix.html

    svd = mat.computeSVD(k=5, computeU=True)
    U = svd.U       # The U factor is a RowMatrix.
    # The singular values are stored in a local dense vector.
    s = svd.s
    V = svd.V       # The V factor is a local dense matrix.

    # Write SVD time to log and restart timer
    svd_time = timeit.default_timer() - start_time
    mylogger.info("{}; {}; {}; {}; {}".format(
        "svd", iteration, instances, svd_time, run_id))
    #---------------------------SVD------------------------------------

    #----------------------PCA---------------------------
    # start_time = timeit.default_timer()

    # # Compute the top 4 principal components.
    # # Principal components are stored in a local dense matrix.
    # pca = mat.computePrincipalComponents(4)

    # # Project the rows to the linear space spanned by the top 4 principal components.
    # projected = mat.multiply(pca)
    # pca_time = timeit.default_timer() - start_time

    # mylogger.info("{}; {}; {}; {}; {}".format(
    #     "pca", iteration, instances, pca_time, run_id))

    #---------------------PCA-----------------------------

    #---------------------- Transpose ---------------------------
    # start_time = timeit.default_timer()

    # transposed = coord_matrix.transpose()
    # transposed_rdd = transposed.entries

    # try:
    #     mylogger.debug("Number of rows: {}".format(transposed_rdd.count()))

    #     transpose_time = timeit.default_timer() - start_time
    #     mylogger.info("{}; {}; {}; {}; {}".format(
    #         "transpose", iteration, instances, transpose_time, run_id))
    #     start_time = timeit.default_timer()
    # except AttributeError as e:
    #     mylogger.warn("Failed transpose.")

    #----------------------Transpose---------------------------

    # Stop SparkContext and run Java sGC
    spark.sparkContext._jvm.System.gc()
    spark.stop()

    # Also run Python GC.
    collected = gc.collect()
    mylogger.debug("Garbage collector: collected %d objects." % collected)
